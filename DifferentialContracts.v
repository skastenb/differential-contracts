Require Import CoqDL.syntax.expressions.
Require Import CoqDL.semantics.dynamic_semantics.
Require Import CoqDL.semantics.static_sem.
Require Import CoqDL.checker.checker.
Require Import List.
Require Export Contracts.

Set Printing Coercions.

Axiom in_dec_KAssignable : forall (x : KAssignable) (d : alphabet), {x ∈ d} + { x ∉ d}.

Instance KAssignable_Reals : NonEmptyDecidableVariable (R * R) KAssignable := {
  any_value := (0 , 0)%R ;
  (* eq_dec_ident := KAssignable_dec ; *)
  in_dec_ident := in_dec_KAssignable ;
}.

Definition to_alphabet (f : @FCset KAssignable) : (@alphabet KAssignable):=
  match f with
  | FCS_finite l => fun (x : KAssignable) => List.In x l
  | FCS_infinite l => fun (x : KAssignable) => ~ List.In x l
  end.

Definition prog_in_alphabet (d : alphabet) (p : Program) :=
  to_alphabet (all_vars_program p) ⊆ d.

Definition formula_in_alphabet (d : alphabet) (f : Formula) :=
  to_alphabet (all_vars_formula f) ⊆ d.

Definition KState := (KAssignable -> R).

Definition value : Type := ( R * R ).
Definition ident : Type := KAssignable.

Definition eval {d : alphabet} (t : behavior d) (x : KAssignable) : value :=
  match in_dec_ident x d with
                     | left x_in_d => t (exist _ x x_in_d)
                     | right _ => any_value
  end.

Definition to_transition {d : alphabet} (t : behavior d) : KState * KState:=
  (fun (x : KAssignable) => fst (@eval d t x),
  fun (x : KAssignable) => snd (@eval d t x)).

Definition mkbehavior {d : alphabet} (prestate poststate : KState) : behavior d :=
  fun var => let (x, _ ):= var in (prestate x, poststate x).

Definition to_component
  {d : alphabet} (I : interpretation) (p : Program) (p_in_alphabet: prog_in_alphabet d p) : component d :=
  fun (t : behavior d) =>
  let (prestate, poststate) := @to_transition d t in
  dynamic_semantics_program I p prestate poststate.

(* Definition sat_program (p : Program) (t : behavior) := *)
(*   let (prestate, poststate) := to_transition t in *)
(*   dynamic_semantics_program I p prestate poststate. *)

Section alphabet_defined.

Variable I : interpretation.
Variable d : (@alphabet KAssignable).
Definition assertion := (@assertion value ident d).
Definition contract := (@contract value ident d).
Definition behavior := (@behavior value ident d).
Definition prog_in_d := (@prog_in_alphabet d).


(* Definition prog2contracts *)
(*   (a g : Program) (a_in_d : prog_in_d a) (g_in_d : prog_in_d g) : contract := *)
(*   mkContract d (to_component a a_in_d) (to_component g g_in_d). *)

Lemma satisfies_DSF  (p : Program) (F : Formula) (p_in_d : prog_in_d p) :
  sequent_true_I {| seq_hyps := emHyps ; seq_concl := KFbox  p F |} I ->
  forall t : behavior, t ∈ (to_component I p p_in_d) ->
  DSF F I (snd (to_transition t)).
Proof.
  unfold to_component, In.
  intros H t.
  destruct (to_transition t) as [prestate poststate].
  assert (DSF (KFbox p F) I prestate) as H0.
    apply H.
    simpl.
    tauto.
  apply H0.
Qed.

Lemma satisfies_DSF2 (p : Program) (F : Formula) (p_in_d : prog_in_d p) :
  sequent_true_I {| seq_hyps := emHyps ; seq_concl := KFimply (KFbox p KFtrue) F |} I ->
  forall t : behavior, t ∈ (to_component I p p_in_d) ->
  DSF F I (fst (to_transition t)).
Proof.
  unfold to_component, In.
  intros H t.
  destruct (to_transition t) as [prestate poststate].
  assert (DSF (KFimply (KFbox p KFtrue) F) I prestate) as H0.
    apply H.
    simpl.
    tauto.
  assert ((DSF (KFbox p KFtrue) I prestate) -> (DSF F I prestate)) as H1.
    generalize H0.
    unfold DSF.
    unfold dynamic_semantics_formula.
    tauto.
  intros H5.
  apply H1.
  generalize H5.
  unfold DSF.
  unfold dynamic_semantics_formula.
  unfold TrueFormulaSem.
  simpl.
  tauto.
Qed.

Lemma nin_dom_const (t : behavior) :
  let (prestate, poststate) := to_transition t in
  forall y : KAssignable,
  y ∉ d -> prestate y = poststate y.
Proof.
  intros y y_not_in_d.
  unfold eval.
  simpl.
  case (in_dec_KAssignable y d).
  + intro y_in_d.
    exfalso.
    apply y_not_in_d ; assumption.
  + tauto.
Qed.

Fixpoint fresh_kassignable_list (n : nat) (l : list KAssignable) : list KAssignable :=
  match n with
  | 0 => nil
  | S m => let (x, _):= (fresh_kassignable l) in x :: (fresh_kassignable_list m ( x :: l))
  end.

  Section finite_alphabet.

Variable d_finite : Finite d.

Definition dl := elements_of d d_finite.

Definition dl' : list KAssignable :=
   fresh_kassignable_list (length dl) dl.

Definition dl'_Variable : list KVariable :=
   map KAssignable2variable dl'.

Lemma KAssignVar_dl'_aux : forall n l,
  map KAssignVar (map KAssignable2variable (fresh_kassignable_list n l)) =
  fresh_kassignable_list n l.
Proof.
  intros n.
  induction n.
  simpl ; tauto.
  simpl.
  intros l.
  rewrite (IHn (KAssignVar
        (variable
           (String (Ascii.Ascii false false false true true true true false)
             (append_string_list (map KAssignable2string l)))) :: l)).
             reflexivity.
Qed.

Lemma KAssignVar_dl' : (map KAssignVar dl'_Variable) = dl'.
Proof.
  exact (KAssignVar_dl'_aux (Datatypes.length dl) dl).
Qed.
Lemma fresh_notin_alphabet_aux : forall (x : KAssignable) (l : list KAssignable) (n : nat),
  List.In x (fresh_kassignable_list n l) -> ~ List.In x l.
Proof.
  intros.
  revert l H.
  induction n.
  - simpl.
    intuition.
  - intros l.
    unfold fresh_kassignable_list.
    destruct (fresh_kassignable l) as [y yp].
    simpl.
    intros [ yx_eq | Hl].
    + subst.
      assumption.
    + assert (~ List.In x (y :: l)).
        apply IHn.
        assumption.
      intro.
      apply H.
      right.
      assumption.
Qed.

Lemma fresh_notin_alphabet : forall x : KAssignable, List.In x dl' -> x ∉ d.
Proof.
  intros.
  unfold dl' in H.
  generalize (Enum _ d d_finite).
  generalize (fresh_notin_alphabet_aux x dl (length dl)).
  intros.
  apply H0 in H.
  intro.
  rewrite H1 in H2.
  apply H.
  assumption.
Qed.

Lemma ndp_fresh_list : forall n l,
  NoDup (fresh_kassignable_list n l).
Proof.
  intros n.
  induction n.
  - simpl.
    intros _.
    apply NoDup_nil.
  - intros l.
    unfold fresh_kassignable_list.
    assert (exists y yp, existT _ y yp = fresh_kassignable l) as [y [yp Hy]].
    { eexists.
      eexists.
      tauto.
    }
    rewrite <- Hy.
    apply NoDup_cons_iff.
    split.
    + fold fresh_kassignable_list.
      intro.
      apply fresh_notin_alphabet_aux in H.
      apply H.
      simpl ; tauto.
    + fold fresh_kassignable_list.
      apply IHn.
Qed.

Lemma ndp_dl' : NoDup dl'.
Proof.
  apply ndp_fresh_list.
Qed.

Fixpoint KFeq_list (la : list KAssignable) ( lb : list KAssignable) : Formula :=
  match (la, lb) with
  | (nil, _::_) => KFfalse
  | (_::_, nil) => KFfalse
  | (nil, nil) => KFtrue
  | (h1::t1, h2::t2) => KFand (KFequal h1 h2) (KFeq_list t1 t2)
  end.

Definition value_d (s : KState) : list R :=
  map (fun (x : KAssignable) => s x) dl.

Definition KFrefine(a b : Program) : Formula :=
   KFforallVars dl'_Variable
   (KFimply (KFdiamond a (KFeq_list dl dl'))
            (KFdiamond b (KFeq_list dl dl'))).

Lemma FCset_app_or : forall (a1 a2 : EAssignables) (x : KAssignable),
        x ∈ to_alphabet (EAssignables_app a1 a2) <->
        x ∈ to_alphabet a1 \/ x ∈ to_alphabet a2.
Proof.
 unfold EAssignables_app, fcset_app, In.
 intros a1 a2 x.
 destruct a1.
 - destruct a2.
   + simpl.
     split.
     * apply in_app_or.
     * apply in_or_app.
   + simpl.
     rewrite in_minus_dec.
     tauto.
 - destruct a2.
   + simpl.
     rewrite in_minus_dec.
     tauto.
   + simpl.
     rewrite in_inter_dec.
     tauto.
Qed.

Lemma nin_vars : forall (a : Program) (s f : KState) (x : KAssignable),
   ~(x ∈ (to_alphabet (all_vars_program a))) -> dynamic_semantics_program I a s f -> s x = f x.
Proof.
  introv x_nin_a dsp_asf.
  generalize (bound_effect_lemma a I s f dsp_asf).
  unfold equal_states_on_complement.
  intros eq_on_compl.
  specialize (eq_on_compl x).
  apply eq_on_compl.
  unfold in_eassignables, in_fcset.
  unfold  all_vars_program in x_nin_a.
  rewrite FCset_app_or in x_nin_a.
  destruct (bound_vars_program a).
  - destruct (in_dec KAssignable_dec x l).
    + exfalso.
      apply x_nin_a.
      right.
      tauto.
    + reflexivity.
  - destruct (in_dec KAssignable_dec x l).
    + reflexivity.
    + exfalso.
      apply x_nin_a.
      right.
      tauto.
Qed.

Lemma bound_effect_on_d (a : Program) (a_in_d : prog_in_d a)
  (s f : KState) (x : KAssignable) :
  ~(x ∈ d) -> dynamic_semantics_program I a s f -> s x = f x.
Proof.
  intros Hx.
  apply nin_vars.
  intros x_in_a.
  apply Hx.
  apply a_in_d.
  assumption.
Qed.

Lemma upd_state_eq_list (f : KState) : forall l l',
  Datatypes.length l = Datatypes.length l' ->
  dynamic_semantics_formula I (KFeq_list l l') f <->
  map f l = map f l'.
Proof.
  induction l.
  - intros l' l'_nil.
    symmetry in l'_nil.
    rewrite length_zero_iff_nil in l'_nil.
    subst.
    simpl.
    unfold TrueFormulaSem.
    tauto.
  - intros l' ll'_len.
    induction l'.
    + simpl.
      unfold FalseFormulaSem.
      split ; try tauto.
      intro H.
      symmetry in H.
      apply nil_cons in H.
      assumption.
    + simpl.
      assert (Datatypes.length l = Datatypes.length l') as Hll'_len.
      { simpl in ll'_len.
        injection ll'_len.
        tauto. }
      split.
      * intros [fa_fa0_eq dsfl].
        f_equal.
        assumption.
        rewrite <- IHl ; assumption.
      * intros Heq.
        injection Heq.
        rewrite IHl ; try assumption.
        tauto.
Qed.

Lemma FCset_app_subset : forall (a1 a2 : EAssignables),
  to_alphabet (EAssignables_app a1 a2) ⊆ d <-> to_alphabet a1 ⊆ d /\ to_alphabet a2 ⊆ d.
Proof.
  intros.
  split.
  - intros H1.
    split;
      intros x x_in_a1;
      apply H1;
      apply FCset_app_or;
      tauto.
  - intros [H1 H2].
    intros x x_in_app.
    apply FCset_app_or in x_in_app.
    destruct x_in_app ; [apply H1 | apply H2] ; assumption.
Qed.

Definition EAssignables_d : EAssignables :=
  FCS_finite dl.

Lemma in_fcset_In (f : FCset) (x : KAssignable) :
  in_fcset KAssignable_dec x f = true <-> x ∈ to_alphabet f.
Proof.
  destruct f.
  - rewrite in_fcset_finite.
    reflexivity.
  - simpl.
    case in_dec.
    + split.
      * intuition.
      * unfold In.
        intro.
        contradiction.
    + tauto.
Qed.

Lemma fcset_subset_Subseteq :
  forall f g, fcset_subset KAssignable_dec f g = true <-> to_alphabet f ⊆ to_alphabet g.
Proof.
  intros f g.
  unfold fcset_subset.
  destruct f.
  - destruct g.
    + simpl.
      rewrite included_dec_prop.
      reflexivity.
    + simpl.
      rewrite disj_dec_prop.
      reflexivity.
  - destruct g.
    + simpl.
      unfold SubsetEq, In.
      split.
      * intuition.
      * intros x_sub.
        exfalso.
        cut (exists x, ~ List.In x l /\ ~ List.In x l0).
        { intros [x [x_nin_l x_nin_l0]] ; intuition. }
        generalize (fresh_kassignable (List.app l l0)).
        intros [x xp].
        exists x.
        intuition.
    + simpl.
      rewrite included_dec_prop.
      unfold SubsetEq, In.
      split.
      * intuition.
      * intros Hsub x x_in_l0.
        specialize (Hsub x).
        tauto.
Qed.

Lemma fcset_subset_d:
  forall f,  to_alphabet f ⊆ d -> fcset_subset KAssignable_dec f EAssignables_d = true .
Proof.
  intros f f_sub_d.
  apply fcset_subset_Subseteq.
  intros x x_in_f.
  apply (Enum _ d d_finite).
  apply (f_sub_d x x_in_f).
Qed.

Lemma fresh_list_notin : forall(x : KAssignable) (n : nat) (l : list KAssignable),
  List.In x (fresh_kassignable_list n l) -> ~ List.In x l.
Proof.
  intros x n.
  induction n.
  - simpl.
    tauto.
  - intros l.
    unfold fresh_kassignable_list.
    assert (exists y yp, existT _ y yp = fresh_kassignable l) as [y [yp Hy]].
    { eexists.
      eexists.
      tauto.
    }
    rewrite <- Hy.
    intros x_in_f.
    apply in_inv in x_in_f.
    destruct x_in_f.
    + subst.
      assumption.
    + specialize (IHn _ H).
      intros x_in_l.
      apply IHn.
      apply in_cons.
      assumption.
Qed.

Lemma disjoint_fresh_d :
  disjoint (vars2assign dl'_Variable) dl.
Proof.
  unfold disjoint.
  unfold vars2assign.
  rewrite KAssignVar_dl'.
  intros x x_in_f.
  apply (fresh_list_notin x _ dl x_in_f).
Qed.

Lemma equal_states_on_map : (forall s s' l, equal_states_on s s' l <-> map s l = map s' l).
Proof.
  intros s s' l.
  unfold equal_states_on.
  split.
  - intro H.
    apply map_ext_in.
    assumption.
  - induction l.
    + simpl ; tauto.
    + repeat rewrite map_cons.
      intros H x x_in_al.
      inversion H.
      destruct x_in_al as [xa_eq | x_in_l].
      * subst.
        assumption.
      * apply IHl ; assumption.
Qed.

Theorem coincidence_program_on_d (P : Program) (p_in_d : prog_in_d P) (s s' f: KState) :
  (map s dl = map s' dl) ->
  dynamic_semantics_program I P s f ->
  exists f',  dynamic_semantics_program I P s' f' /\
  map f dl = map f' dl.
Proof.
  introv mapss' dsp_sf.
  generalize (coincidence_program P s s' f I I EAssignables_d).
  intro H.
  destruct H as [w' [Hdsp_sx eq_ww']].
  - apply fcset_subset_d.
    apply FCset_app_subset in p_in_d.
    tauto.
  - apply equal_states_on_ea_assigns2ext.
    rewrite equal_states_on_map.
    assumption.
  - apply equal_interpretations_on_ext_refl.
  - assumption.
  - exists w'.
    split ; try assumption.
    apply equal_states_on_map.
    apply equal_states_on_ea_assigns2ext.
    apply (equal_states_on_ea_eassignables_eq f w' (EAssignables_app EAssignables_d (must_bound_vars_program P))) ; try assumption.
    unfold eassignables_eq, fcset_eq.
    intros a.
    rewrite in_eassignables_app_true_iff.
    repeat rewrite in_fcset_In.
    generalize (must_bound_vars_program_subset_bound_vars_program P).
    rewrite fcset_subset_Subseteq.
    intro must_bound_subset_bound.
    unfold In.
    simpl.
    rewrite<- (Enum _ d d_finite).
    split.
    + intros [ in_d | in_p ].
      * assumption.
      * apply p_in_d.
        apply FCset_app_or.
        right.
        apply must_bound_subset_bound.
        apply in_p.
    + tauto.
Qed.

Theorem coincidence_program_d : forall s f w P,
  to_alphabet (all_vars_program P) ⊆ d ->
  dynamic_semantics_program I P (upd_list_state s (combine dl'_Variable (value_d f))) w ->
  (forall x, x ∈ d -> w x = f x) ->
  (forall x, ~(x ∈ d) -> s x = f x) ->
  dynamic_semantics_program I P s f.
Proof.
  introv Hav Hdsp Hd Hnd.
  generalize (coincidence_program P (upd_list_state s (combine dl'_Variable (value_d f))) s w I I EAssignables_d).
  intros H.
  destruct H as [w' [Hdsp_sx eq_ww']].
  - apply fcset_subset_d.
    apply FCset_app_subset in Hav.
    tauto.
  - apply equal_states_on_ea_assigns2ext.
    apply equal_states_on_upd_list_state_if_disjoint.
    apply disjoint_fresh_d.
  - apply equal_interpretations_on_ext_refl.
  - assumption.
  - assert (f = w').
    + apply functional_extensionality.
      intros x.
      case (in_dec_KAssignable x d).
      * intros x_in_d.
        rewrite <- (Hd x x_in_d).
        apply eq_ww'.
        unfold in_eassignables.
        rewrite in_fcset_app_true_iff.
        left.
        unfold EAssignables_d.
        simpl.
        case (in_dec KAssignable_dec).
        { reflexivity. }
        { rewrite <- (Enum _ d d_finite).
          intuition. }
      * intros x_nin_d.
        rewrite <- (Hnd x x_nin_d).
        apply nin_vars with P.
        { intros x_in_P.
          apply x_nin_d.
          apply Hav.
          assumption. }
        assumption.
    + rewrite H.
      assumption.
Qed.

Lemma upd_list_state_diff : forall s sub x,
  ~(List.In x (map KAssignVar (map  fst sub))) ->
  s x = upd_list_state s sub x.
Proof.
  intros.
  induction sub.
  - simpl ; tauto.
  -
  simpl.
  destruct a as [v r].
  rewrite upd_state_diff.
  apply IHsub.
  revert H.
  simpl.
  intuition.
  revert H.
  simpl.
  intuition.
Qed.

Lemma map_combine : forall (A B : Type) (al : list A) (bl : list B),
  Datatypes.length al = Datatypes.length bl -> map fst (combine al bl) = al.
Proof.
  intros A B al.
  induction al.
    simpl ; tauto.
  intros bl.
  induction bl.
    simpl.
    intuition.
  simpl.
  intros Hlen.
  rewrite IHal.
  reflexivity.
  auto.
Qed.



Lemma len_fresh_kassignable_list : forall (n : nat) (l : list KAssignable),
  Datatypes.length (fresh_kassignable_list n l) = n.
Proof.
  induction n.
  - simpl.
    reflexivity.
  - intros l.
    simpl.
    rewrite IHn.
    reflexivity.
Qed.

Lemma len_fresh_d :
  Datatypes.length dl = Datatypes.length dl'.
Proof.
  unfold dl'.
  rewrite len_fresh_kassignable_list.
  reflexivity.
Qed.

Lemma len_value_d_fresh: forall f : KState,
  Datatypes.length (value_d f) = Datatypes.length dl'_Variable.
Proof.
  intros f.
  unfold value_d, dl'_Variable, dl'.
  rewrite map_length.
  rewrite map_length.
  rewrite len_fresh_kassignable_list.
  reflexivity.
Qed.

Lemma map_upd_list_state_combine :
  forall (v w : KState) (l : list KVariable) (l': list KAssignable),
  NoDup (map KAssignVar l) ->
  Datatypes.length l = Datatypes.length l' ->
  map (upd_list_state v (combine l (map w l'))) (map KAssignVar l) =
  map w l'.
Proof.
  intros v w l l' ndl Hlen.
  apply eq_maps3.
    rewrite map_length.
    assumption.
  intros x y xy_in.
  revert l Hlen xy_in ndl.
  induction l'.
  - simpl.
    intros.
    apply length_zero_iff_nil in Hlen.
    subst.
    simpl in xy_in.
    tauto.
  - intros.
    destruct l.
    + simpl in Hlen.
      exfalso.
      tauto.
    + simpl.
      simpl in xy_in.
      destruct xy_in.
      * inversion H .
        subst.
        apply upd_state_same.
      * rewrite upd_state_diff.
        rewrite IHl'.
        reflexivity.
        simpl in Hlen.
        injection Hlen.
        tauto.
        assumption.
        apply NoDup_cons_iff in ndl.
        tauto.
        {
          apply NoDup_map_inv in ndl.
          apply NoDup_cons_iff in ndl.
          destruct ndl as [k_nin_l _].
          apply in_combine_l in H.
          intro k_eq_x.
          subst.
          apply k_nin_l.
          apply in_map_iff in H.
          destruct H.
          destruct H.
          injection H.
          intros xk_eq.
          subst.
          simpl.
          fold map.
          tauto.
          }
Qed.

Open Scope list_scope.
Lemma fcset_sub : forall (fs : EAssignables),
  to_alphabet fs ⊆ d <->
  eassignables_subset fs EAssignables_d = true.
Proof.
  intros fs.
  unfold eassignables_subset, fcset_subset, EAssignables_d.
  destruct fs.
  - unfold SubsetEq.
    simpl.
    rewrite included_dec_prop.
    unfold subset.
    split ; intros.
    + rewrite <- (Enum _ d d_finite).
      apply H ; assumption.
    + rewrite (Enum _ d d_finite).
      apply H ; assumption.
  - simpl.
    split; intros.
    + exfalso.
      unfold SubsetEq in H.
      assert (exists x xp, existT _ x xp = fresh_kassignable (l ++ dl)) as [x [xp Hr]].
        eexists.
        eexists.
        tauto.
      specialize (H  x).
      rewrite (Enum _ d d_finite x) in H.
      unfold In in H.
      apply xp.
      rewrite in_app_iff.
      right.
      apply H.
      intro.
      apply xp.
      rewrite in_app_iff.
      left.
      assumption.
    + exfalso.
      apply diff_false_true ; assumption.
Qed.

Lemma upd_list_state_eq (s : KState) (l : list KVariable) :
  upd_list_state s (combine l (map s (map KAssignVar l))) = s.
Proof.
  induction l.
  - simpl.
    reflexivity.
  - simpl.
    rewrite IHl.
    apply functional_extensionality.
    intros x.
    case (KAssignable_dec x a).
    + intro.
      subst.
      apply upd_state_same.
    + intro xa_neq.
      apply upd_state_diff.
      firstorder.
Qed.

Lemma ext_in_map :
    forall (A B : Type)(f g:A->B) l, map f l = map g l -> forall a, List.In a l -> f a = g a.
Proof. intros A B f g l; induction l; intros [=] ? []; subst; auto. Qed.

Lemma KFdiam_eqlist_rewrite (a : Program) (a_in_d : prog_in_d a)  (s : KState) :
  dynamic_semantics_formula I (KFdiamond a (KFeq_list dl (dl'))) s ->
  exists f, dynamic_semantics_program I a s f /\ map f dl = map f (dl').
Proof.
  simpl.
  intros [f [dsf_eql dspa_sf]].
  exists f.
  split ; try assumption.
  apply upd_state_eq_list.
  apply len_fresh_d.
  assumption.
Qed.

Lemma KFrefine_rewrite (a b : Program) (a_in_d : prog_in_d a) (b_in_d : prog_in_d b) :
  forall s f : KState,
  dynamic_semantics_formula I (KFrefine a b) s ->
  dynamic_semantics_program I a s f ->
  map f dl = map f (dl') ->
  exists f, dynamic_semantics_program I b s f /\
    map f dl = map f (dl').
Proof.
  intros s f kfre dspaf mapf.
  apply KFdiam_eqlist_rewrite ; try assumption.
  specialize (kfre (map s dl')).
  simpl in kfre.
  fold dynamic_semantics_program in kfre.
  fold dynamic_semantics_formula in kfre.
  simpl.
  assert ((upd_list_state s (combine dl'_Variable (map s dl'))) = s) as s_r.
  { rewrite <- KAssignVar_dl'.
    apply upd_list_state_eq.
   }
  rewrite s_r in kfre.
  apply kfre.
  { rewrite map_length.
    unfold dl'_Variable.
    rewrite map_length.
    reflexivity.
  }
  exists f.
  split.
  - rewrite upd_state_eq_list ; try assumption.
    apply len_fresh_d.
  - assumption.
Qed.

Theorem refine_KFrefine
  (a b : Program) (a_in_d : prog_in_d a) (b_in_d : prog_in_d b) :
  (forall preS : KState, dynamic_semantics_formula  I (KFrefine a b) preS) ->
  to_component I a a_in_d ⊆ to_component I b b_in_d.
Proof.
  intros HKref.
  unfold to_component.
  intros t.
  unfold In.
  assert (exists s f,to_transition t = (s,f)) as [s [f Ht]].
    { eexists ; eexists ; tauto. }
  assert (exists s', s' = (upd_list_state s (combine dl'_Variable (map f dl)))) as [s' srt].
  { eexists. tauto. }
  assert (map s' dl = map s dl) as map_s's.
  { apply map_ext_in.
    intros x x_in_d.
    symmetry.
    subst.
    apply upd_list_state_diff.
    rewrite map_combine.
    rewrite KAssignVar_dl'.
    unfold dl in x_in_d.
    rewrite <- Enum in x_in_d.
    intro.
    apply fresh_notin_alphabet in H.
    contradiction.
    rewrite map_length.
    symmetry.
    unfold dl'_Variable.
    rewrite map_length.
    apply len_fresh_d.
    }
  rewrite Ht.
  intros dspa.
  specialize (HKref s').
  generalize (coincidence_program_on_d a a_in_d s s' f).
  intros H.
  destruct H as [f' [dsp_s'f' map_ff']] ; try assumption.
  + symmetry ; assumption.
  apply (KFrefine_rewrite a b a_in_d b_in_d s' f') in HKref ; try assumption.
  - destruct HKref as [f'' [dsp_bs'f'' mapf'']].
    generalize (coincidence_program_on_d b b_in_d s' s f'' map_s's dsp_bs'f'').
    intros H.
    destruct H as [f''' [dsp_sf''' map_f''f''']] ; try assumption.
    cut (f = f''').
    + intro ; subst ; assumption.
    + apply functional_extensionality.
      intros x.
      destruct (in_dec_KAssignable x d).
      * apply ext_in_map with dl.
        { rewrite <- map_f''f'''.
          rewrite mapf''.
          assert (map f'' dl' = map s' dl').
          { symmetry.
            apply map_ext_in.
            intros y y_fresh.
            apply bound_effect_on_d with b ; try assumption.
            apply fresh_notin_alphabet ; assumption.
          }
          rewrite H.
          symmetry.
          subst.
          rewrite <-  KAssignVar_dl'.
          apply map_upd_list_state_combine.
          rewrite KAssignVar_dl'.
          apply ndp_dl'.
          symmetry.
          unfold dl'_Variable.
          rewrite map_length.
          apply len_fresh_d.
        }
        apply (Enum _ d d_finite) ; assumption.
      * apply eq_trans with (s x).
        symmetry.
        apply bound_effect_on_d with a ; assumption.
        apply bound_effect_on_d with b ; assumption.
  - rewrite <- map_ff'.
    assert (map f' dl' = map s' dl').
    { symmetry.
      apply map_ext_in.
      intros x x_fresh.
      apply bound_effect_on_d with a ; try assumption.
      apply fresh_notin_alphabet ; assumption.
    }
    rewrite H.
    subst.
    simpl.
    symmetry.
    rewrite <-  KAssignVar_dl'.
    apply map_upd_list_state_combine.
    rewrite KAssignVar_dl'.
    apply ndp_dl'.
    symmetry.
    unfold dl'_Variable.
    rewrite map_length.
    apply len_fresh_d.
Qed.

End finite_alphabet.

End alphabet_defined.
