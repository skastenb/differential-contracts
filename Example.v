(* Require Import dynamic_semantics. *)
(* Require Import static_sem. *)
Require Import checker.
Require Import VCT.Contracts.
Require Import VCT.Logic_Contract.
Require Import DifferentialContracts.
Require Import AbstractPrograms.
(* Require Import List. *)
(* Import ListNotations. *)

Definition a  : list Prop := [ True ].


Section Example.

Variable I : interpretation.

Definition h : KAssignable := KAssignVar (variable "h").
Definition h' : KAssignable := KAssignDiff h.
Definition h'' : KAssignable := KAssignDiff h'.
Definition v : KAssignable := variable "v".
Definition d : alphabet := add v ( add h'' (add h' (add h (@emptyset KAssignable)))).

Definition aSaturate := @saturateF _ _ _ (abstract_program I) d.
Definition aCompose := @composeF _ _ _ (abstract_program I) d.
Definition to_contract := @contract_of _ _ _ (abstract_program I) d.

Axiom fd : Finite d.

Lemma h_in_d : h ∈ d.
Proof.
  firstorder.
Qed.

Lemma v_in_d : v ∈ d.
Proof.
  firstorder.
Qed.

Definition h_v := exist _ h h_in_d.
Definition h'_v := exist _ h h_in_d.
Definition v_v := exist _ v v_in_d.
Definition HMax : Term := KTnumber (KTNreal 20).
Definition HLimit : Term := KTnumber (KTNreal 15).

Definition alpha : Program := KPcompose
                                (KPassignAny h)
                                (KPchoice
                                  (KPtest (KFless (KTread h) HLimit))
                                  (KPassign v (KTnumber (KTNreal 0)))).

Lemma alpha_in_dom : prog_in_alphabet d alpha.
Proof.
  intros x .
  unfold In.
  simpl.
  firstorder.
Qed.

Definition alpha_component := to_component I alpha alpha_in_dom.

Definition ca_assume  := aTrue d.

Definition atom_1_f : atom_ := postF ((KFimply
      (KFgreaterEqual (KTread h) HLimit)
      (KFequal v (KTnumber (KTNreal 0))))).
Lemma atom_1_in_d : atom_in_alphabet d atom_1_f.
Proof. firstorder. Qed.
Definition atom_1 := exist _ atom_1_f atom_1_in_d.
Definition ca_guarantee : aProgram d := [[ atom_1 ]].

Definition ca_abstract_contract : contractF d := ContractF d ca_assume ca_guarantee.

Theorem ca_implements_alpha :
  (forall preS, dynamic_semantics_formula I
    (KFrefine d fd alpha
      (to_program d fd (G d (aSaturate ca_abstract_contract))))
    preS)
  -> implements d alpha_component (to_contract ca_abstract_contract).
Proof.
  apply (proof_trans I d fd).
Qed.

Definition beta : Program := KPcompose
  (KPassignAny v)
  (KPodeSystem
    (ODEatomic (ODEsing (KAssignDiff h) (KTread v)))
    (KFtrue)
  ).

Definition atom_2_f := preF (KFlessEqual h HMax).
Lemma atom_2_in_d : atom_in_alphabet d atom_2_f.
Proof. firstorder. Qed.
Definition atom_2 := exist _ atom_2_f atom_2_in_d.

Definition cb_assume : aProgram d := [[ atom_2 ; atom_1 ]].

Definition atom_3_f := postF (KFlessEqual h HMax).
Lemma atom_3_in_d : atom_in_alphabet d atom_3_f.
Proof. firstorder. Qed.
Definition atom_3 := exist _ atom_3_f atom_3_in_d.

Definition cb_guarantee : aProgram d := [[ atom_3 ]].

Definition cb_contract := ContractF d cb_assume cb_guarantee.

Lemma beta_in_dom : prog_in_alphabet d beta.
Proof. firstorder. Qed.

Definition beta_component := to_component I beta beta_in_dom.

Theorem beta_implements_cb :
  implements d beta_component (@contract_of _ _ _ (abstract_program I) d cb_contract).
Proof.
  apply (proof_trans I d fd).
  admit.
Admitted.

Definition gamma :=
  KPloop
    (KPcompose
      (KPchoice
        (KPtest (KFlessEqual (KTread h) HMax))
        (KPassign v (KTnumber (KTNreal 0%R)))
      )
      (KPodeSystem
        (ODEatomic (ODEsing (KAssignDiff h) (KTread v)))
        (KFless h HMax)
      )
    ).

Lemma gamma_in_dom : to_alphabet (all_vars_program gamma) ⊆ d.
Proof. firstorder. Qed.

Definition gamma_component := to_component I gamma gamma_in_dom.

Definition cg_contract : contractF d :=
  aCompose ca_abstract_contract cb_contract.

Lemma cg_contract_reduction :
  aSaturate cg_contract =
  aSaturate (ContractF d [[ atom_2 ]] [[ atom_1 ; atom_3 ]]).
Proof.
Admitted.
  (* unfold cg_contract, composeF. *)
  (* unfold ca_abstract_contract, cb_contract. *)
  (* unfold ca_assume, ca_guarantee. *)
  (* unfold cb_assume, cb_guarantee. *)
  (* f_equal. *)
  (* - unfold saturateF. *)
  (*   simpl. *)
  (*   fold atom_3. *)
  (*   fold atom_2. *)
  (*   fold atom_1. *)
  (*   unfold e_or. *)
  (*   intuition. *)
  (*   hnf. *)
  (* inversion. *)
  (* simpl. *)
  (* hnf. *)

(* Definition cg_assume : aProgram d := (preF d (KFlessEqual h HMax)::nil)::nil. *)

(* Definition cg_guarantee : sprogram d := (postF d (KFimply *)
(*           (KFlessEqual h HMax) *)
(*           (KFequal v (KTnumber (KTNreal 0%R))) *)
(*         ) *)
(*         :: *)
(*         postF d (KFlessEqual h HMax) :: nil)::nil. *)


Theorem gamma_implments_cg :
  (forall preS : KState,
  dynamic_semantics_formula I
    (KFrefine d fd gamma (to_program d fd (G d (aSaturate cg_contract))))
    preS)
  ->
  implements d gamma_component (to_contract cg_contract).
Proof.
  apply (proof_trans I d fd).
Qed.

End Example.

