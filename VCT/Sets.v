(* Set.v ---
 *
 * Filename: Set.v
 * Description: Draft of Set Library
 * Author: Benoît Boyer
 * Maintainer:
 * Created: ven. janv. 29 08:49:43 2021 (+0100)
 * Version:
 *)



(* The type Γ is the type of the elements in the set.
 * We assume The type is provided as type inhabited with discriminable elements.
 * In particular, it is useful to prove the last Theorem [add_remove].
 *)

Require Import FunctionalExtensionality.
Require Import PropExtensionality.
Require Import List.
Require Import Classical.

(* Class Discr(Γ: Type) := { *)
(*       eq_dec : forall x y: Γ, { x = y } + { x <> y } *)
(*   }. *)

(* TODO: Classes for Groups, Ordered sets, Monoids lattices? *)

Module CoqSet.

  Definition set (Γ: Type) : Type := Γ -> Prop.

  Definition In {Γ: Type} (x: Γ) (s: set Γ) : Prop := s x.
  Notation "x ∈ s" := (@In _ x s) (at level 70, no associativity).
  Notation "x ∉ s" := (~ @In _ x s) (at level 70, no associativity).

  Definition SubsetEq {Γ: Type} (s1 s2: set Γ) : Prop :=
    forall x: Γ, x ∈ s1 -> x ∈ s2.
  Notation "u ⊆ v" := (@SubsetEq _ u v) (at level 70, no associativity).

  Definition Eq {Γ: Type} (s1 s2: set Γ) : Prop :=
    forall x: Γ, x ∈ s1 <-> x ∈ s2.
  Notation "u == v" := (@Eq _ u v) (at level 70, no associativity).

  Theorem Eq_extensionality: forall {Γ: Type} (s1 s2 : set Γ),
    s1 == s2 -> s1 = s2.
  Proof.
    unfold set, Eq, In.
    intros.
    apply functional_extensionality.
    intro x.
    apply propositional_extensionality.
    apply H.
  Qed.

  Theorem SubsetEq_Refl {Γ: Type} :
    forall s: set Γ,     s ⊆ s.
  Proof.
    firstorder.
  Qed.

  Theorem SubsetEq_Trans {Γ: Type} :
    forall s1 s2 s₃: set Γ,      s1 ⊆ s2 -> s2 ⊆ s₃ -> s1 ⊆ s₃.
  Proof.
    firstorder.
  Qed.

  Theorem SubsetEq_Asym {Γ: Type} :
    forall s1 s2: set Γ, s1 ⊆ s2 -> s2 ⊆ s1 -> s1 == s2.
  Proof.
    firstorder.
  Qed.

  Theorem Eq_Refl {Γ: Type} :
    forall s: set Γ,   s == s.
  Proof.
    firstorder.
  Qed.

  Theorem Eq_Sym {Γ: Type} :
    forall s1 s2: set Γ, s1 == s2 -> s2 == s1.
  Proof.
    firstorder.
  Qed.

  Theorem Eq_Trans {Γ: Type} :
    forall  s1 s2 s₃: set Γ,     s1 == s2 -> s2 == s₃ -> s1 == s₃.
  Proof.
    firstorder.
  Qed.
  (** TODO: Once we proved Eq is an equivalence relation (RST), it could be
            nice to register a monoid with the Boolean operations: helpful for
            manual rewriting in proof. *)

  (** The empty set *)
  Definition emptyset {Γ: Type} : set Γ := fun _: Γ => False.

  Notation "∅" := (@emptyset _).
  (* TODO: No need to set the level here. To be confirmed by looking
     at the definition of the notation for List.nil ([]) for
     instance... *)

  Theorem emptyset_is_empty {Γ: Type} :
    forall x: Γ,        x ∉ ∅.
  Proof.
    firstorder.
  Qed.

  (** The universe set *)
  Definition Univ {Γ: Type} : set Γ :=
    fun (_: Γ) => True.

  Theorem Univ_is_full {Γ: Type} :
    forall x: Γ,        x ∈ Univ.
  Proof.
    firstorder.
  Qed.

  Theorem emptyset_is_bot {Γ: Type} :
    forall s: set Γ,        ∅ ⊆ s.
  Proof.
    firstorder.
  Qed.

  Theorem Univ_is_top {Γ: Type} :
    forall s: set Γ,     s ⊆ Univ.
  Proof.
    firstorder.
  Qed.

  Definition is_empty {Γ: Type} (s : set Γ) :=
    forall x: Γ, x ∉ s.

  Definition is_not_empty {Γ: Type} (s : set Γ) :=
    exists x: Γ, x ∈ s.

  Theorem is_not_empty_neg_empty {Γ: Type} (s : set Γ) :
    is_not_empty s -> ~ @is_empty Γ s.
  Proof.
    unfold is_not_empty, is_empty.
    intros [x x_in_s] H.
    specialize (H x x_in_s).
    contradiction.
  Qed.

  (** Boolean [union] and [inter]section *)
  Definition union {Γ: Type} (s1 s2: set Γ) : set Γ :=
    fun x: Γ => x ∈ s1 \/ x ∈ s2.
  Notation "u ∪ v" := (@union _ u v) (at level 61, left associativity).
  (* TODO: Level 61 is set arbitrarily, must be checked against ( + ) for instance... *)

  Definition inter {Γ: Type} (s1 s2: set Γ) : set Γ :=
    fun x: Γ => x ∈ s1 /\ x ∈ s2.
  Notation "u ∩ v" := (@inter _ u v) (at level 51, left associativity).
  (* TODO: Level 50 is set arbitrarily, must be checked against ( * ) for instance... *)

  Theorem union_commut {Γ: Type} :
    forall s1 s2: set Γ,         s1 ∪ s2 == s2 ∪ s1.
  Proof.
    firstorder.
  Qed.

  Theorem union_assoc {Γ: Type} :
    forall s1 s2 s₃: set Γ,      s1 ∪ (s2 ∪ s₃) == (s1 ∪ s2) ∪ s₃.
  Proof.
    firstorder.
  Qed.

  Theorem union_emptyset {Γ: Type} :
    forall s: set Γ,   s ∪ ∅ == s.
  Proof.
    firstorder.
  Qed.

  Theorem union_Univ {Γ: Type} :
    forall s: set Γ,   s ∪ Univ == Univ.
  Proof.
    firstorder.
  Qed.

  Theorem union_idem {Γ: Type} :
    forall s: set Γ,   s ∪ s == s.
  Proof.
    firstorder.
  Qed.

  Theorem union_monotonic_l {Γ: Type} :
    forall s1 s2: set Γ,       s1 ⊆ s1 ∪ s2.
  Proof.
    firstorder.
  Qed.

  Theorem union_monotonic_r {Γ: Type} :
    forall s1 s2: set Γ,       s2 ⊆ s1 ∪ s2.
  Proof.
    firstorder.
  Qed.

  Theorem union_monotonic_lr {Γ: Type} :
    forall s1 s2 s₃ s₄: set Γ, s1 ⊆ s₃ -> s2 ⊆ s₄ -> s1 ∪ s2 ⊆ s₃ ∪ s₄.
  Proof.
    firstorder.
  Qed.

  Theorem in_union_monotonic_l {Γ: Type} :
    forall (s1 s2: set Γ) (x : Γ),       x ∈ s1 -> x ∈ s1 ∪ s2.
  Proof.
    firstorder.
  Qed.

  Theorem inter_commut {Γ: Type} :
    forall s1 s2: set Γ,       s1 ∩ s2 == s2 ∩ s1.
  Proof.
    firstorder.
  Qed.

  Theorem inter_assoc {Γ: Type} :
    forall s1 s2 s₃: set Γ,    s1 ∩ (s2 ∩ s₃) == (s1 ∩ s2) ∩ s₃.
  Proof.
    firstorder.
  Qed.

  Theorem inter_emptyset {Γ: Type} :
    forall s: set Γ,   s ∩ ∅ == ∅.
  Proof.
    firstorder.
  Qed.

  Theorem inter_Univ {Γ: Type} :
    forall s: set Γ,   s ∩ Univ == s.
  Proof.
    firstorder.
  Qed.

  Theorem inter_idem {Γ: Type} :
    forall s: set Γ,   s ∩ s == s.
  Proof.
    firstorder.
  Qed.

  Theorem inter_monotonic_l {Γ: Type} :
    forall s1 s2: set Γ,       s1 ∩ s2 ⊆ s1.
  Proof.
    firstorder.
  Qed.

  (** Boolean *)
  Definition diff {Γ: Type} (s1 s2: set Γ) : set Γ :=
    fun x: Γ => x ∈ s1 /\ x ∉ s2.
  Notation "u \ v" :=  (@diff _ u v) (at level 61, left associativity).

  Theorem diff_emptyset1 {Γ: Type} :
    forall s: set Γ,     s \ ∅ == s.
  Proof.
    firstorder.
  Qed.

  Theorem diff_emptyset2 {Γ: Type} :
    forall s: set Γ,     ∅ \ s == ∅.
  Proof.
    firstorder.
  Qed.

  Theorem union_diff {Γ: Type} :
    forall s1 s2 s₃: set Γ,      (s1 ∪ s2) \ s₃ == (s1 \ s₃) ∪ (s2 \ s₃).
  Proof.
    firstorder.
  Qed.

  Theorem diff_union {Γ: Type} :
    forall s1 s2 s₃: set Γ,      s1 \ (s2 ∪ s₃) ⊆ (s1 \ s2) ∪ (s1 \ s₃).
  Proof.
    firstorder.
  Qed.

  Theorem diff_inter {Γ: Type} :
    forall s1 s2 s₃: set Γ,      (s1 \ s2) ∩ (s1 \ s₃) ⊆ s1 \ (s2 ∩ s₃).
  Proof.
    firstorder.
  Qed.

  Theorem inter_diff {Γ: Type} :
    forall s1 s2 s₃: set Γ,      (s1 ∩ s2) \ s₃ == (s1 \ s₃) ∩ (s2 \ s₃).
  Proof.
    firstorder.
  Qed.

  (* Boolean [compl]] *)
  Definition compl {Γ: Type} (s: set Γ) : set Γ :=
    fun (x: Γ) => x ∉ s.
  Notation "¬ s" := (@compl _ s) (at level 41, no associativity).
  (* TODO: check level *)


  Theorem compl_Univ {Γ: Type} : ¬ (@Univ Γ) == ∅.
  Proof.
    firstorder.
  Qed.

  Theorem compl_emptyset {Γ: Type} : ¬ ∅ == @Univ Γ.
  Proof.
    firstorder.
  Qed.

  (** Operator [add] and [singleton] *)
  Definition add {Γ: Type} (x: Γ) (s: set Γ) : set Γ :=
    fun y => y = x \/ y ∈ s.

  Definition singleton {Γ: Type} (x: Γ) : set Γ := add x ∅.

  Notation "{ x }" := (@singleton _ x).
  (* TODO: evaluation the term "{ x } ∪ s" returns undefined notation :-(
           See the theorems [add_union] and [remove_diff] below *)

  Theorem In_add {Γ: Type} :
    forall (x: Γ) (s: set Γ),       x ∈ (add x s).
  Proof.
    firstorder.
  Qed.

  Theorem add_union  {Γ: Type} :
    forall (x: Γ) (s: set Γ),       add x s == union {x} s. (* { x } ∪ s *)
  Proof.
    firstorder.
  Qed.

  Theorem add_commut {Γ: Type} :
    forall (x y: Γ) (s: set Γ),     add x (add y s) == add y (add x s).
  Proof.
    firstorder.
  Qed.

  Theorem add_idem {Γ: Type} :
    forall (x: Γ) (s: set Γ),       add x (add x s) == add x s.
  Proof.
    firstorder.
  Qed.


  (** Operator [remove] *)
  Definition remove {Γ: Type} (x: Γ) (s: set Γ) : set Γ :=
    fun y => y <> x /\ y ∈ s.


  Theorem In_remove {Γ: Type} :
    forall (x: Γ) (s: set Γ),       x ∉ (remove x s).
  Proof.
    firstorder.
  Qed.

  Theorem remove_diff {Γ: Type} :
    forall (x: Γ) (s: set Γ),       remove x s == diff s {x}.
  Proof.
    firstorder.
  Qed.

  Theorem remove_commut {Γ: Type} :
    forall (x y: Γ) (s: set Γ),     remove x (remove y s) == remove y (remove x s).
  Proof.
    firstorder.
  Qed.

  Theorem remove_idem {Γ: Type} :
    forall (x: Γ) (s: set Γ),       remove x (remove x s) == remove x s.
  Proof.
    firstorder.
  Qed.

  Theorem remove_add {Γ: Type} :
    forall (x: Γ) (s: set Γ),       remove x (add x s) == remove x s.
  Proof.
    firstorder.
  Qed.

  Theorem add_remove {Γ} :
    forall (x: Γ) (s: set Γ),       add x (remove x s) == add x s.
  Proof.
    intros x s y; split.
    firstorder.
    destruct (classic (x = y)).
    firstorder.
    firstorder.
  Qed.

  Section Classical.


Lemma morgan {Γ: Type} : forall s1 s2 : set  Γ, ¬ s1 ∩ ¬ s2 = ¬ (s1 ∪ s2).
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma morgan2 {Γ: Type}: forall s1 s2 : set  Γ, ¬ (s1 ∩ b) = ¬ s1 ∪ ¬ b.
Proof.
  intros.
  apply Eq_extensionality.
  split ; try firstorder.
  case classic with (x ∈ s1).
  intro.
  right.
  tauto.
  left.
  tauto.
Qed.

Lemma inter_commut_eq {Γ: Type} : forall s1 s2: set  Γ, s1 ∩ s2 = s2 ∩ s1.
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma inter_distrib {Γ: Type} : forall a b c : set  Γ, a ∩ (b ∪ c) = (a ∩ b) ∪ (a ∩ c).
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma inter_anoa {Γ: Type} : forall a : set  Γ, a ∩ ¬ a = emptyset.
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma comp_comp {Γ: Type} : forall a : set  Γ, ¬ (¬ a) = a.
Proof.
  intro.
  apply Eq_extensionality.
  split ;
  case classic with (x ∈ a) ; firstorder.
Qed.

Lemma union_distrib {Γ: Type} : forall (a b c : set  Γ), (a ∩ b) ∪ c = (a ∪ c) ∩ (b ∪ c).
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma union_anoa {Γ: Type} : forall a : set  Γ, a ∪ ¬ a = Univ.
Proof.
  intros.
  apply Eq_extensionality.
  split.
  firstorder.
  firstorder.
  case classic with (x ∈ a) ; firstorder.
Qed.

Lemma inter_univ {Γ: Type} : forall a : set  Γ, a ∩ Univ = a.
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma inter_abac {Γ: Type} : forall (a b c : set  Γ), a ∩ b ∩ a ∩ c = a ∩ (b ∩ c).
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma inter_inter_commut {Γ: Type} : forall (a b c : set  Γ), a ∩ (b ∩ c) = a ∩ (c ∩ b).
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma inter_union_abb {Γ: Type} : forall (a b : set  Γ), (a ∩ b) ∪ b =  b.
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.

Lemma inter_monotonic_lr {Γ: Type} : forall (a b c d : set  Γ), a ⊆ b -> c ⊆ d -> a ∩ c ⊆ b ∩ d.
Proof.
  firstorder.
Qed.

Lemma inter_assoc_eq {Γ: Type} : forall (a b c : set  Γ), a ∩ b ∩ c = a ∩ (b ∩ c).
Proof.
  intros.
  apply Eq_extensionality.
  firstorder.
Qed.
  End Classical.

  Section Finite.
    Axiom Finite : forall {Γ : Type}  (s: set Γ),  Prop.
    Axiom elements_of : forall {Γ : Type} (s : set Γ) (fs : Finite s),
      list Γ.
    Axiom Enum : forall (Γ : Type) (s : set Γ) (fs : Finite s),
      let l := elements_of s fs in forall (x : Γ), x ∈ s <-> List.In x l.
  End Finite.

End CoqSet.
(* Set.v ends here *)
