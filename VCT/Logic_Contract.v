Require Import Sets.
(* Require Import Classical. *)
Require Import Contracts.
Include CoqSet.

Class AlphabetizedExpression  (value ident : Type) (expr : set ident -> Type)  := {
  e_and : forall d : set ident, expr d -> expr d -> expr d ;
  e_or : forall d : set ident, expr d -> expr d -> expr d ;
  e_not : forall d : set ident, expr d -> expr d ;
  sat : forall d : alphabet, (@behavior value ident d) -> expr d -> Prop ;

  sat_e_not : forall (d : set ident) (s : behavior d) (e : expr d),
    ~ sat d s e <-> sat d s (e_not d e) ;
  sat_e_and : forall (d : set ident) (s : behavior d) (e1 e2 : expr d),
    sat d s e1 /\ sat d s e2 <-> sat d s (e_and d e1 e2) ;
  sat_e_or : forall (d : set ident) (s : behavior d) (e1 e2 : expr d),
    sat d s e1 \/ sat d s e2 <-> sat d s (e_or d e1 e2) ;
  sat_dec : forall (d : set ident) (s : behavior d) (e : expr d),
    sat d s e \/ ~ sat d s e ;
}.

(* Section class_both. *)

(* Class both_theo := { *)
(*   B : Type ; *)
(*   ident' : Type ; *)

(*   any_B : B ; *)
(*   eq_dec_ident : forall x y : ident', {x = y} + {x <> y} ; *)
(*   in_dec_ident : forall (v : ident') (d : set ident'), {v ∈ d} + {v ∉ d} ; *)

(*   expr' : set ident' -> Type ; *)
(*   e_and' : forall d : set ident', expr' d -> expr' d -> expr' d ; *)
(*   e_or' : forall d : set ident', expr' d -> expr' d -> expr' d ; *)
(*   e_not' : forall d : set ident', expr' d -> expr' d ; *)
(*   behavior' : set ident' -> Type ; *)
(*   sat' : forall d : set ident', behavior' d -> expr' d -> Prop *)
(* }. *)

(* End class_both. *)

Section ContractF.

Context {value ident : Type}.
Context {expr : set ident -> Type}.
Context {abstract_ae : AlphabetizedExpression value ident expr}.

Variable d : set ident.

Record contractF := ContractF {A : expr d ; G : expr d}.

Definition sat_contract (s : behavior d) (cf : contractF) :=
  sat d s (cf.(A)) -> sat d s (cf.(G)).

Definition provides_contract (s : behavior d) (cf : contractF) :=
  sat d s (cf.(A)).

Definition saturateF (cf1 : contractF) : contractF :=
  ContractF (cf1.(A)) (e_or d (cf1.(G)) (e_not d (cf1.(A)))).

(* Ici on a besoin d'environment *)
Definition refinesF (cf1 cf2 : contractF) :=
  let cf1' := saturateF cf1 in
  let cf2' := saturateF cf2 in
  forall (s : behavior d), (sat d s cf2'.(A) ->
  sat d s cf1'.(A)) /\ (sat d s cf1'.(G) -> sat d s cf2'.(G)).
  (* (forall (s : behavior d), sat_contract s cf1 -> sat_contract s cf2) *)
  (* /\ (forall (s : behavior d), provides_contract s cf2 -> provides_contract s cf1). *)


Definition composeF (cf1 cf2 : contractF) : contractF :=
  let cf1' := saturateF cf1 in
  let cf2' := saturateF cf2 in
  let g := e_and d (cf1'.(G)) (cf2'.(G)) in
  let a := e_or d (e_and d (cf1'.(A)) (cf2'.(A))) (e_not d g) in
  ContractF a g.

Definition glbF (cf1 cf2 : contractF) : contractF :=
  let cf1' := saturateF cf1 in
  let cf2' := saturateF cf2 in
  let a := e_or d cf1'.(A) cf2'.(A) in
  let g := e_and d cf1'.(G) cf2'.(G) in
  ContractF a g.

End ContractF.

Section Equiv_Contracts.

Context {value ident : Type}.
Context {expr : set ident -> Type}.
Context {abstract_nedv : NonEmptyDecidableVariable value ident}.
Context {abstract_ae : AlphabetizedExpression value ident expr}.

Variable d : (@alphabet ident).

Definition assertion_of (formula : expr d) : assertion d :=
  fun e => sat d e formula.

Definition contract_of (cf : contractF d) : contract d :=
  mkContract d (assertion_of cf.(A d)) (assertion_of cf.(G d)).

Lemma f2a_sat : forall (f : expr d) (s : behavior d), assertion_of f s <-> sat d s f.
Proof.
  firstorder.
Qed.

Lemma f2a_neg : forall (f : expr d) (s : behavior d),
  (assertion_of (e_not d f)) s <-> ( ¬ (assertion_of f)) s.
Proof.
  split ; rewrite f2a_sat ; apply sat_e_not.
Qed.

Lemma f2a_and : forall (f1 f2 : expr d) (s : behavior d),
  (assertion_of (e_and d f1 f2)) s <-> ((assertion_of f1) s /\ (assertion_of f2) s).
Proof.
  split ; repeat rewrite f2a_sat ; apply sat_e_and.
Qed.

Lemma f2a_or : forall (f1 f2 : expr d) (s : behavior d),
  (assertion_of (e_or d f1 f2)) s <-> ((assertion_of f1) s \/ (assertion_of f2) s).
Proof.
  split ; repeat rewrite f2a_sat ; apply sat_e_or.
Qed.

Notation "c1 == c2" := (equiv _ c1 c2).

(* Lemma Morgan2 : forall P Q, ~ (P /\ Q) <-> ~ P \/ ~ Q. *)
(* Proof. *)
(*   firstorder. *)
(*   case classic with (P) ; case classic with (Q) ; tauto. *)
(* Qed. *)

Theorem saturateF_correct : forall (cf : contractF d),
  contract_of (saturateF d cf) == saturate d (contract_of cf).
Proof.
  intros cf.
  cut ((forall (s : behavior d), satisfies d s (contract_of (saturateF d cf))  <->
  satisfies d s (saturate d (contract_of cf))) /\
  forall (s : behavior d), s ∈ Contracts.A d (contract_of (saturateF d cf)) <->
  s ∈ Contracts.A d (saturate d (contract_of cf))).
  { intros [H He].
    unfold equiv.
    split.
    - rewrite refines_correct.
      split.
      + unfold implements.
        intros σ Hs s s_in_σ.
        apply H.
        apply Hs.
        assumption.
      + intros e Hs s s_in_e.
        apply He.
        apply Hs.
        assumption.
    - rewrite refines_correct.
      split.
      + unfold implements.
        intros σ Hs s s_in_σ.
        apply H.
        apply Hs.
        assumption.
      + intros e Hs s s_in_e.
        apply He.
        apply Hs.
        assumption.
 }
  split.
  - split.
    + unfold contract_of, saturateF, saturate, satisfies.
      unfold union, inter, compl, In.
      simpl.
      rewrite f2a_or.
      rewrite f2a_neg.
      unfold compl, In.
      tauto.
    + unfold contract_of, saturateF, saturate, satisfies.
      unfold union, inter, compl, In.
      simpl.
      rewrite f2a_or.
      rewrite f2a_neg.
      unfold compl, In.
      tauto.
  - unfold contract_of, saturateF, saturate, satisfies.
    unfold union, inter, compl, In.
    simpl.
    tauto.
Qed.

Lemma sat_contract_contract_of : forall (s : behavior d) (cf : contractF d),
  sat_contract d s cf <-> satisfies d s (contract_of cf).
Proof.
  unfold satisfies, contract_of, sat_contract, union, compl, In.
  intros s [af gf].
  simpl.
  unfold assertion_of.
  split.
  intro H.
  case (sat_dec d s af) ; tauto.
  tauto.
Qed.

Lemma providesF_contract_of : forall (s : behavior d) (cf : contractF d),
 provides_contract d s cf <-> s ∈ assertion_of (cf.(A d)).
Proof.
 firstorder.
Qed.

Theorem refinesF_correct : forall (cf1 cf2 : contractF d),
  refines d (contract_of cf1) (contract_of cf2) <-> refinesF d cf1 cf2.
Proof.
  split.
  unfold contract_of, refines, refinesF, saturateF.
  simpl.
  intros [Ha Hg] s.
  split.
  + intros.
    rewrite <- f2a_sat.
    apply Ha.
    assumption.
  + repeat rewrite <- sat_e_or.
    repeat rewrite <- f2a_sat.
    repeat rewrite  f2a_neg.
    unfold union, inter, compl, In.
    intro.
    cut (s ∈ ¬ assertion_of (A d cf2) ∪ assertion_of (G d cf2)).
    - unfold union, inter, compl, In.
      tauto.
    - apply Hg.
      unfold union, inter, compl, In.
      tauto.
  + unfold contract_of, refines, refinesF, saturateF.
    simpl.
    intros H.
    split.
    - intros s.
      destruct (H s).
      unfold In.
      repeat rewrite f2a_sat.
      assumption.
    - intros s.
      destruct (H s).
      unfold union, inter, compl, In.
      repeat rewrite f2a_sat.
      generalize H1.
      repeat rewrite <- sat_e_or.
      repeat rewrite <- sat_e_not.
      tauto.
Qed.

Theorem composeF_correct : forall (cf1 cf2 : contractF d),
  contract_of (composeF d cf1 cf2) == compose d (contract_of cf1) (contract_of cf2).
Proof.
  intros [a1 g1] [a2 g2].
  unfold composeF, compose, contract_of.
  simpl.
  apply refines_antisym.
  - unfold refines.
    simpl.
    split.
      + intro s.
        unfold inter, union, SubsetEq, compl, In.
        repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
        unfold inter, union, SubsetEq, compl, In).
        tauto.
      + intro s.
        unfold inter, union, SubsetEq, compl, In.
        repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
        unfold inter, union, SubsetEq, compl, In).
        tauto.
  - unfold refines.
    simpl.
    split.
      + intro s.
        unfold inter, union, SubsetEq, compl, In.
        repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
        unfold inter, union, SubsetEq, compl, In).
        tauto.
      + intro s.
        unfold inter, union, SubsetEq, compl, In.
        repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
        unfold inter, union, SubsetEq, compl, In).
        tauto.
Qed.

Theorem glbF_correct : forall (cf1 cf2 : contractF d),
  contract_of (glbF d cf1 cf2) == glb _ (contract_of cf1) (contract_of cf2).
Proof.
  intros [a1 g1] [a2 g2].
  unfold glbF, glb, contract_of.
  simpl.
  apply refines_antisym.
    - unfold refines.
      simpl.
      split.
        + intro s.
          unfold inter, union, SubsetEq, compl, In.
          repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
          unfold inter, union, SubsetEq, compl, In).
          tauto.
        + intro s.
          unfold inter, union, SubsetEq, compl, In.
          repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
          unfold inter, union, SubsetEq, compl, In).
          tauto.
    - unfold refines.
      simpl.
      split.
        + intro s.
          unfold inter, union, SubsetEq, compl, In.
          repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
          unfold inter, union, SubsetEq, compl, In).
          tauto.
        + intro s.
          unfold inter, union, SubsetEq, compl, In.
          repeat (try rewrite f2a_neg ; try rewrite f2a_and ; try rewrite f2a_or ;
          unfold inter, union, SubsetEq, compl, In).
          tauto.
Qed.

End Equiv_Contracts.
