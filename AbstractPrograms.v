Require Import CoqDL.syntax.expressions.
Require Import CoqDL.semantics.dynamic_semantics.
Require Import CoqDL.semantics.static_sem.
Require Import CoqDL.checker.checker.
Require Import List.
Require Import Contracts.
Require Import Logic_Contract.
Require Import DifferentialContracts.
Require Import Classical.

Set Printing Coercions.

Section AbstractProgram.

Axiom in_dec_KAssignable : forall (x : KAssignable) (d : alphabet), {x ∈ d} + { x ∉ d}.

Variable  I : interpretation.

Definition list_alphabet (l : list ident) : alphabet :=
  fun (x : ident) => List.In x l.

Fixpoint assignAny_aux (l : list ident) : Program :=
  match l with
  | nil => KPtest KFtrue
  | h :: t => KPcompose (KPassignAny h) (assignAny_aux t)
  end.

Definition assignAny (d : alphabet) (fd : Finite d) : Program :=
  assignAny_aux (elements_of d fd).

Lemma KPcompose_in_alphabet_and (d : alphabet) (p q : Program) :
  prog_in_alphabet d p -> prog_in_alphabet d q ->
  prog_in_alphabet d (KPcompose p q).
Proof.
  unfold prog_in_alphabet, all_vars_program.
  simpl.
  repeat rewrite FCset_app_subset.
  intros [fp bp] [fq bq].
  split.
  - split ; try assumption.
    intros x.
    rewrite<- in_fcset_In.
    rewrite in_eassignables_remove_eassignables.
    rewrite in_fcset_In.
    intros [x_in_fvq _].
    apply fq.
    assumption.
  - tauto.
Qed.

Theorem assignAny_aux_in_d (d : alphabet) (l : list ident) :
  (forall x, List.In x l -> x ∈ d) ->
  prog_in_alphabet d (assignAny_aux l).
Proof.
  induction l.
  - simpl.
    intros  _.
    unfold prog_in_alphabet, all_vars_program.
    simpl.
    intros x.
    intuition.
  - intros Hal.
    simpl.
    apply KPcompose_in_alphabet_and.
    + intros x.
      unfold In.
      simpl.
      specialize (Hal x).
      intros [ax | ?] ; try contradiction.
      subst.
      apply Hal.
      left.
      reflexivity.
    + apply IHl.
      intros x x_in_l.
      apply Hal.
      right.
      assumption.
Qed.

Theorem assignAny_in_d (d : alphabet) (fd : Finite d) :
  prog_in_d d (assignAny d fd).
Proof.
  unfold assignAny.
  apply assignAny_aux_in_d.
  intros x.
  rewrite (Enum _ d fd).
  tauto.
Qed.

Lemma assignAny_dsp_strong_list : forall (l : list ident) (d : alphabet) (preS postS : KState),
  (forall (x : KAssignable), x ∉ d -> preS x = postS x) ->
  (forall (x : KAssignable), x ∈ d <-> List.In x l) ->
  (forall (y  : KAssignable), ~ List.In y  l -> preS y = postS y)
  -> dynamic_semantics_program I (assignAny_aux l) preS postS.
Proof.
  induction l.
  + unfold dynamic_semantics_program.
    simpl.
    intros.
    split.
    apply functional_extensionality.
    intros x.
    apply H1.
    tauto.
    unfold TrueFormulaSem.
    tauto.
  + intros.
    simpl.
    exists (upd_state preS a (postS a)).
    split.
    exists (postS a).
    unfold differ_state_except.
    split.
    - intros y ay_neq.
      rewrite (upd_state_diff _ a _ _ ay_neq).
      reflexivity.
    - rewrite (upd_state_same _ a _).
      reflexivity.
    apply (IHl (list_alphabet l)).
    * intros x x_nin_dl.
      case (KAssignable_dec a x).
        + intros ax_eq.
          rewrite ax_eq.
          rewrite upd_state_same.
          reflexivity.
        + intros ax_neq.
          rewrite (upd_state_diff _ a _ _ ax_neq).
          apply H1.
          intros [xa_eq | x_in_l].
          tauto.
          apply x_nin_dl.
          unfold list_alphabet, In.
          assumption.
    * unfold list_alphabet, In.
      reflexivity.
    * intros y y_nin_l.
      case (KAssignable_dec a y).
      + intros ay_eq.
        rewrite ay_eq.
        apply upd_state_same.
      + intros ay_neq.
        rewrite (upd_state_diff _ a _ _ ay_neq).
        apply H1.
        intros [ay_eq | y_in_l].
        apply ay_neq ; assumption.
        apply y_nin_l ; assumption.
Qed.

Lemma assignAny_dsp_strong : forall (d : alphabet) (fd : Finite d) (t : behavior d),
  let (preS, postS) := to_transition t in
  (forall ( y : ident), ~ List.In y  (elements_of d fd) -> preS y = postS y)
  -> dynamic_semantics_program I (assignAny d fd) preS postS.
Proof.
  intros d fd.
  generalize (Enum _ d fd).
  intro Henum.
  simpl in Henum.
  intros t.
  assert (exists (pres posts : KState), to_transition t = (pres, posts)).
    eexists.
    eexists.
    reflexivity.
  destruct H as [preS [postS Hrt]].
  rewrite Hrt.
  apply (assignAny_dsp_strong_list (elements_of d fd) d) .
  intros x x_nin_d.
  assert (eval t x = any_value).
    unfold eval.
    destruct in_dec_ident.
    intuition.
    reflexivity.
  unfold to_transition in Hrt.
  inversion Hrt.
  rewrite H.
  simpl.
  reflexivity.
  assumption.
Qed.

Lemma assignAny_dsp : forall (d : alphabet) (fd : Finite d) (t : behavior d),
  let (preS, postS) := to_transition t in
  dynamic_semantics_program I (assignAny d fd) preS postS.
Proof.
  intros d fd t.
  apply assignAny_dsp_strong.
  unfold eval.
  intros y Hy.
  destruct in_dec_ident.
  + exfalso.
    apply Hy.
    apply (Enum _ d fd).
    intuition.
  + reflexivity.
Qed.

Inductive atom_ :=
  | preF : Formula -> atom_
  | postF : Formula -> atom_.

Definition atom_in_alphabet (d : alphabet) (e : atom_) : Prop :=
  match e with
  | preF f => formula_in_alphabet d f
  | postF f => formula_in_alphabet d f
  end.

Definition atom (d : alphabet) := {e | atom_in_alphabet d e}.

Definition aProgram (d : alphabet) :=
 list (list (atom d)).

Definition aTrue (d : alphabet) : aProgram d := (@nil (atom d))::nil.
Definition aFalse (d : alphabet) : aProgram d := (@nil (list (atom d))).

Definition aProgram_sat_atom (d : alphabet) (t : behavior d) (e : atom d) : Prop :=
  let (prestate, poststate) := to_transition t in
  match proj1_sig e with
  | preF f => dynamic_semantics_formula I f prestate
  | postF f => dynamic_semantics_formula I f poststate
  end.

Fixpoint aProgram_sat_aux (d : alphabet) (t : behavior d) (sp_and : list (atom d)) : Prop :=
  match sp_and with
  | nil => True
  | h :: q =>  aProgram_sat_atom d t h /\ aProgram_sat_aux d t q
  end.

Fixpoint aProgram_sat (d : alphabet) (s : behavior d) (sp : aProgram d) : Prop :=
  match sp with
  | nil => False
  | h :: t => aProgram_sat_aux d s h \/ aProgram_sat d s t
  end.

Definition aProgram_or (d : alphabet) (a b : aProgram d) : aProgram d :=
  a ++ b.

Fixpoint aProgram_and_aux (d : alphabet) (l : list ( atom d)) (prog : aProgram d) : aProgram d :=
  match prog with
  | nil => nil
  | h :: t => (l ++ h) :: aProgram_and_aux d l t
  end.

Fixpoint aProgram_and (d : alphabet) (a b : aProgram d) : aProgram d :=
  match a with
  | nil => []
  | h :: t => aProgram_or d (aProgram_and_aux d h b)  (aProgram_and d t b)
  end.

Lemma KFnot_in_d (d : alphabet) (f : Formula) (pf : formula_in_alphabet d f) :
  formula_in_alphabet d (KFnot f).
Proof.
  tauto.
Qed.

Definition aProgram_not_atom (d : alphabet) (e : atom d) : atom d :=
  match e with
  | exist (preF f) pf => exist _ (preF (KFnot f)) (KFnot_in_d d f pf)
  | exist (postF f) pf => exist _ (postF (KFnot f)) (KFnot_in_d d f pf)
  end.

Fixpoint aProgram_not_aux (d : alphabet) (l : list (atom d)) : aProgram d :=
  match l with
  | nil => []
  | h :: t => ((aProgram_not_atom d h)::nil) :: (aProgram_not_aux d t)
  end.

Fixpoint aProgram_not (d : alphabet) (sp : aProgram d) : aProgram d :=
  match sp with
  | nil => nil :: nil
  | h :: t => aProgram_and d (aProgram_not_aux d h) (aProgram_not d t)
  end.

Lemma aProgram_or_correct : forall (d : alphabet) (s : behavior d) (sp1 sp2 : aProgram d),
  aProgram_sat d s sp1 \/ aProgram_sat d s sp2 <-> aProgram_sat d s (aProgram_or d sp1 sp2).
Proof.
  intros.
  induction sp1.
  + simpl.
    tauto.
  + unfold aProgram_or.
    simpl.
    tauto.
Qed.

Lemma sataux_and : forall (d : alphabet) (s : behavior d) (a b : list ( atom d)),
  aProgram_sat_aux d s (a ++ b) <-> aProgram_sat_aux d s a /\ aProgram_sat_aux d s b.
Proof.
  intros.
  induction a.
  + simpl ; tauto.
  + simpl.
    rewrite IHa.
    tauto.
Qed.

Lemma sat_sand_aux :
  forall (d : alphabet) (s : behavior d) (sp : aProgram d) (a : list ( atom d)),
  aProgram_sat d s (aProgram_and_aux d a sp) <-> (aProgram_sat_aux d s a /\ aProgram_sat d s sp).
Proof.
  intros.
  induction sp.
  + simpl ; tauto.
  + simpl.
    rewrite sataux_and.
    rewrite IHsp.
    tauto.
Qed.

Lemma aProgram_and_correct : forall (d : alphabet) (s : behavior d) (sp1 sp2 : aProgram d),
  aProgram_sat d s sp1 /\ aProgram_sat d s sp2 <-> aProgram_sat d s (aProgram_and d sp1 sp2).
Proof.
  intros.
  induction sp1.
  + simpl.
    tauto.
  + simpl.
    rewrite <- aProgram_or_correct.
    rewrite <- IHsp1.
    rewrite sat_sand_aux.
    tauto.
Qed.

Lemma sat_aProgram_not_atom :
  forall (d : alphabet) (s : behavior d) (a : list ( atom d)) (e :  atom d),
  aProgram_sat d s (aProgram_not_aux d (e :: a)) <->
  aProgram_sat_atom d s (aProgram_not_atom d e) \/ aProgram_sat d s (aProgram_not_aux d a).
Proof.
  intros.
  simpl.
  tauto.
Qed.

Lemma sat_aux_sat_atom :
  forall (d : alphabet) (s : behavior d) (a : list ( atom d)) (e :  atom d),
  aProgram_sat_aux d s (e :: a) <-> aProgram_sat_atom d s e /\ aProgram_sat_aux d s a.
Proof.
  simpl.
  tauto.
Qed.

Lemma sat_atom_not :
  forall (d : alphabet) (s : behavior d) (e :  atom d),
  aProgram_sat_atom d s (aProgram_not_atom d e) <-> ~ aProgram_sat_atom d s e.
Proof.
  intros.
  destruct e as [e pe].
  destruct e.
  - simpl; tauto.
  - simpl; tauto.
Qed.

Lemma sat_aux_not : forall (d : alphabet) (s : behavior d) (a : list ( atom d)),
  aProgram_sat d s (aProgram_not_aux d a) <-> ~ aProgram_sat_aux d s a.
Proof.
  intros.
  induction a.
  + simpl ; tauto.
  + rewrite sat_aProgram_not_atom.
    rewrite IHa.
    rewrite sat_aux_sat_atom.
    rewrite sat_atom_not.
    tauto.
Qed.

Lemma aProgram_not_correct : forall (d : alphabet) (s : behavior d) (sp : aProgram d),
  ~ aProgram_sat d s sp <-> aProgram_sat d s (aProgram_not d sp).
Proof.
  intros.
  induction sp.
  + simpl.
    tauto.
  + simpl.
    rewrite <- aProgram_and_correct.
    rewrite sat_aux_not.
    tauto.
Qed.

Axiom aProgram_sat_dec : forall (d : alphabet) (s : behavior d) (sp : aProgram d),
  aProgram_sat d s sp \/ ~ aProgram_sat d s sp.

Instance abstract_program : AlphabetizedExpression (R*R) KAssignable aProgram := {
  e_and := aProgram_and ;
  e_or := aProgram_or ;
  e_not := aProgram_not ;
  sat := aProgram_sat ;
  sat_e_not := aProgram_not_correct ;
  sat_e_or := aProgram_or_correct ;
  sat_e_and := aProgram_and_correct ;
  sat_dec := aProgram_sat_dec ;
}.

Fixpoint flatten_pre (d : alphabet) (l : list (atom d)) : Formula :=
  match l with
  | nil => KFtrue
  | exist (preF f) _ :: t => KFand f (flatten_pre d t)
  | exist (postF _) _ :: t => flatten_pre d t
  end.

Fixpoint flatten_post (d : alphabet) (l : list (atom d)) : Formula :=
  match l with
  | nil => KFtrue
  | exist (preF _) _ :: t => flatten_post d t
  | exist (postF f) _ :: t => KFand f (flatten_post d t)
  end.

Definition to_program_aux (d : alphabet) (fd : Finite d) (s : list (atom d)) : Program :=
      KPcompose
        (KPcompose
          (KPtest (flatten_pre d s))
          (assignAny d fd))
        (KPtest (flatten_post d s))
        .

Fixpoint to_program (d : alphabet) (fd : Finite d) (s : aProgram d) : Program :=
  match s with
  | nil => KPtest KFfalse
  | h :: t => KPchoice (to_program_aux d fd h) (to_program d fd t)
  end.

Lemma KPchoice_in_alphabet_and (d : alphabet) (p q : Program) :
  prog_in_alphabet d (KPchoice p q) <->
  prog_in_alphabet d p /\ prog_in_alphabet d q.
Proof.
  unfold prog_in_alphabet, all_vars_program.
  simpl.
  repeat rewrite FCset_app_subset.
  tauto.
Qed.


Lemma KPtest_in_alphabet (d : alphabet) (f : Formula) :
  formula_in_alphabet d f ->
  prog_in_alphabet d (KPtest f).
Proof.
  unfold prog_in_alphabet, formula_in_alphabet, all_vars_formula, all_vars_program.
  simpl.
  rewrite FCset_app_subset.
  rewrite FCset_app_subset.
  simpl.
  intuition.
  intros x.
  intuition.
Qed.

Lemma KPtest_KFand (d : alphabet) (f g : Formula) :
  prog_in_alphabet d (KPtest (KFand f g)) <->
  prog_in_alphabet d (KPtest f) /\ prog_in_alphabet d (KPtest g).
Proof.
  unfold prog_in_alphabet, all_vars_program.
  simpl.
  repeat rewrite FCset_app_subset.
  intuition.
Qed.

Lemma to_program_aux_in_d (d : alphabet) (fd : Finite d) (l : list (atom d)):
  prog_in_alphabet d (to_program_aux d fd l).
Proof.
  unfold to_program_aux.
  apply KPcompose_in_alphabet_and.
  apply KPcompose_in_alphabet_and.
  - induction l.
    + unfold prog_in_alphabet, all_vars_program.
      simpl.
      intro x ;  intuition.
    + destruct a.
      destruct x.
      * simpl.
        apply KPtest_KFand.
        split ; try assumption.
        unfold atom_in_alphabet in a.
        apply KPtest_in_alphabet.
        assumption.
      * simpl.
        assumption.
  - apply assignAny_in_d.
  - induction l.
    + intros x.
      simpl.
      intuition.
    + destruct a ; destruct x.
      * simpl.
        assumption.
      * simpl.
        apply KPtest_KFand.
        split ; try assumption.
        unfold atom_in_alphabet in a.
        apply KPtest_in_alphabet.
        assumption.
Qed.

Theorem to_program_in_d (d : alphabet) (fd : Finite d) (sp : aProgram d) :
  prog_in_alphabet d (to_program d fd sp).
Proof.
  induction sp.
  - unfold to_program.
    intro x.
    simpl.
    intuition.
  - simpl.
    rewrite KPchoice_in_alphabet_and.
    split.
    + apply to_program_aux_in_d.
    + assumption.
Qed.

Lemma sat_atom_preF (d : alphabet) (fd : Finite d) (f : Formula) (f_in_d : formula_in_alphabet d f) (t : behavior d) :
  let (preS, postS) := to_transition t in
  aProgram_sat_atom d t (exist _ (preF f) f_in_d) <->
  dynamic_semantics_formula I f preS.
Proof.
  unfold aProgram_sat_atom.
  simpl.
  reflexivity.
Qed.

Lemma sat_atom_postF (d : alphabet) (fd : Finite d) (f : Formula) (f_in_d : formula_in_alphabet d f) (t : behavior d) :
  let (preS, postS) := to_transition t in
  aProgram_sat_atom d t (exist _ (postF f) f_in_d) <->
  dynamic_semantics_formula I f postS.
Proof.
  unfold aProgram_sat_atom.
  simpl.
  reflexivity.
Qed.

Ltac simpl_exists :=
  try intuition ;
  repeat
  match goal with
  | H: exists _, _ |- _ =>
    let e := fresh "e" in
    let EH := fresh "EH" in
      destruct H as (e & EH)
  | H: _ /\ _ |- _ =>
    let CH0 := fresh "CH0" in
    let CH := fresh "CH" in
      destruct H as (CH0 & CH)
  end ;
  try tauto.

Lemma to_program_aux_cons (d : alphabet) (fd : Finite d) (l : list (atom d)) (e : atom d) (t :behavior d):
  let (preS, postS) := to_transition t in
  dynamic_semantics_program I
  (to_program_aux d fd (e :: l)) preS postS
  <-> aProgram_sat_atom d t e /\ dynamic_semantics_program I (to_program_aux d fd l) preS postS.
Proof.
  assert (exists (pres posts : KState), to_transition t = (pres, posts)).
    destruct (to_transition t) as (preS, postS).
    exists preS.
    exists postS.
    reflexivity.
  destruct H as [preS [postS Hrt]].
  rewrite Hrt.
  destruct e as [e e_in_d].
  destruct e as [f | f].
  - generalize (sat_atom_preF d fd f e_in_d t).
    rewrite Hrt.
    intros HpreF.
    rewrite HpreF.
    simpl.
    simpl_exists.
    + eexists.
      split.
      eexists.
      eauto.
      eauto.
    + eexists.
      split.
      eexists.
      eauto.
      eauto.
  - generalize (sat_atom_postF d fd f e_in_d t).
    rewrite Hrt.
    intros HpostF.
    rewrite HpostF.
    simpl.
    simpl_exists.
    + subst ; assumption.
    + eexists.
      split.
      eexists.
      eauto.
      eauto.
    + eexists.
      split.
      eexists.
      eauto.
      subst ; tauto.
Qed.

Lemma sat_to_program_sound_aux :
  forall (d : alphabet) (fd : Finite d) (a : list (atom d)) (t : behavior d),
  let (preS, postS) := to_transition t in
  dynamic_semantics_program I (to_program_aux d fd a) preS postS <->
  aProgram_sat_aux d t a.
Proof.
  intros.
  assert (exists (pres posts : KState), to_transition t = (pres, posts)).
    destruct (to_transition t) as (preS, postS).
    exists preS.
    exists postS.
    reflexivity.
  destruct H as [preS [postS Hrt]].
  rewrite Hrt.
  induction a.
  - simpl.
    split ; auto.
    intros _.
    unfold TrueFormulaSem.
    exists postS.
    split ; try tauto.
    exists preS.
    split ; try tauto.
    generalize (assignAny_dsp d fd t).
    rewrite Hrt.
    tauto.
  - generalize(to_program_aux_cons d fd a0 a t).
    rewrite Hrt.
    intro Hspcons.
    rewrite Hspcons.
    rewrite IHa.
    simpl.
    reflexivity.
Qed.

Theorem sat_to_program_sound : forall (d : alphabet) (fd : Finite d) (sp : aProgram d) (t : behavior d),
  let (preS, postS) := to_transition t in
  dynamic_semantics_program I (to_program d fd sp) preS postS <-> aProgram_sat d t sp.
Proof.
  intros.
  induction sp.
  + unfold aProgram_sat.
    destruct (to_transition t) as [preS postS].
    unfold dynamic_semantics_program.
    simpl.
    intuition.
  + unfold aProgram_sat.
    generalize (sat_to_program_sound_aux d fd a t).
    destruct (to_transition t) as [preS postS].
    intros Ha.
    fold aProgram_sat.
    rewrite <- IHsp.
    assert (dynamic_semantics_program I (to_program d fd (a :: sp)) preS postS <->
    dynamic_semantics_program I (to_program_aux d fd a) preS postS \/
    dynamic_semantics_program I (to_program d fd sp) preS postS).
      unfold to_program.
      simpl.
      tauto.
    rewrite H.
    rewrite Ha.
    reflexivity.
Qed.

Theorem proof_trans
  (d : alphabet) (fd : Finite d) (a : Program) (a_in_d : prog_in_d d a)
  (c : contractF d) :
  (forall preS : KState,
  (dynamic_semantics_formula I (KFrefine d fd a (to_program d fd (G d (saturateF d c)))) preS)) ->
  implements d (to_component I a a_in_d) (contract_of d c).
Proof.
  intros H.
  assert (to_alphabet (all_vars_program (to_program d fd (G d (saturateF d c)))) ⊆ d) as Hvarsc.
  {
    apply to_program_in_d.
    }
  assert ( to_component I a a_in_d ⊆ to_component I (to_program d fd (G d (saturateF d c))) Hvarsc) as Href.
    apply (refine_KFrefine I d fd a (to_program d fd (G d (saturateF d c)))) ;
    assumption.
  intros t.
  unfold to_component, In.
  assert (exists preS postS, to_transition t = (preS, postS)) as Hrt.
    eexists.
    eexists.
    reflexivity.
  destruct Hrt as [preS [postS Hrt]].
  rewrite Hrt.
  intros dspa.
  apply sat_contract_contract_of.
  unfold sat_contract.
  assert (aProgram_sat d t (G d (saturateF d c))) as H1.
    generalize (sat_to_program_sound d fd (G d (saturateF d c)) t).
    rewrite Hrt.
    intros H1.
    rewrite <- H1.
    (* unfold refine, component_refines, to_component in Href. *)
    (* unfold SubsetEq, In in Href. *)
    specialize (Href t).
    unfold In, to_component in Href.
    rewrite Hrt in Href.
    apply Href.
    assumption.
  revert H1.
  unfold saturateF.
  simpl.
  rewrite <- aProgram_or_correct.
  rewrite <- aProgram_not_correct.
  intuition.
Qed.

End AbstractProgram.
