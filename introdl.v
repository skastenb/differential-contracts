Require Import checker.
Require Import VCT.Contracts.
Require Import VCT.Logic_Contract.
Require Import DifferentialContracts.
Require Import AbstractPrograms.
Require Import Vector.

Notation "[ ]" := nil (format "[ ]") : vector_scope.
Notation "h :: t" := ( cons _ h _ t) (at level 60, right associativity)
    : vector_scope.
Notation "[ x ]" := (const x 1) : vector_scope. (* Define notation for vector *)

Notation "[ x ; y ; .. ; z ]" := (cons _ x _ (cons _ y _ .. (cons _ z _ (nil _)) ..)) : vector_scope.

Open Scope R_scope.
Open Scope vector_scope.

Variable I : interpretation.


Definition is_equal_KAssignable
  := KAssignable_dec.
Definition h : KAssignable := KAssignVar (variable "h").
Definition h' : KAssignable := KAssignDiff h.
Definition h'' : KAssignable := KAssignDiff h'.

Definition nu : state := fun x : KAssignable => if  is_equal_KAssignable x h then 1 else 0.

Definition theta : Term := KTplus (KTread h) (KTnumber (KTNnat 2)).

Definition v : KAssignable := variable "v".
Definition d : @alphabet KAssignable := add v ( add h'' (add h' (add h (@emptyset KAssignable)))).

Definition aSaturate := @saturateF _ _ _ (abstract_program I) d.
Definition aCompose := @composeF _ _ _ (abstract_program I) d.
Definition to_contract := @contract_of _ _ _ (abstract_program I) d.

Axiom fd : Finite d.

Lemma h_in_d : h ∈ d.
Proof.
  firstorder.
Qed.

Lemma v_in_d : v ∈ d.
Proof.
  firstorder.
Qed.



Definition h_v := exist _ h h_in_d.
Definition h'_v := exist _ h h_in_d.
Definition v_v := exist _ v v_in_d.
Definition HMax : Term := KTnumber (KTNreal 20).
Definition HLimit : Term := KTnumber (KTNreal 15).

Definition h_term : Term := KTread h.

Definition cos : FunctionSymbol := function_symbol "cos".
Definition cos_h : Term := KTfuncOf cos 1 [ h_term ].

Definition phi : Formula := KFless (KTread h) HLimit.

Definition divides : PredicateSymbol := predicate_symbol "divides".
Definition h_even : Formula := KFpredOf divides 2 [h_term ; KTnumber (KTNnat 2)].

Open Scope list_scope.
Definition cos_le_one : Formula := KFforallVars [variable "h"] (KFlessEqual cos_h (KTnumber (KTNnat 1))).

Definition alpha : Program := KPcompose
                                (KPassignAny h)
                                (KPchoice
                                  (KPtest phi)
                                  (KPassign v (KTnumber (KTNreal 0)))).

Definition beta : Program :=
  KPodeSystem
    (ODEatomic (ODEsing (KAssignDiff h) (KTread v)))
    phi.

Check interp_fun_f.
Print TrueFormulaSem.
Print exist.
Print existT.
Print behavior.
Eval compute in behavior.
Definition component_example : component d := fun x : behavior d => True.
